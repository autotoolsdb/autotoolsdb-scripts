#!/usr/bin/perl -w
use strict;
use WWW::Mechanize;
use WWW::Mechanize::FormFiller;
use URI::URL;
use warnings;
use Getopt::Long;
use HTML::TableExtract;
use Data::Dumper;

=pod
    the release should be read from /etc/lsb-release, from the DISTRIB_CODENAME
=cut

sub getRelease
{
    open IN,"/etc/lsb-release";
    while (<IN>)
    {
	if (/DISTRIB_CODENAME=(.+)/)
	{
	    return $1;
	}
    }
    close IN;
}

my $release=getRelease();

warn "detected $release";

=pod
the name of the headerfile to search for 
=cut
my $header="";

GetOptions ('release:s' => \$release, 'header:s' => \$header);

my $agent = WWW::Mechanize->new( autocheck => 1 );
my $formfiller = WWW::Mechanize::FormFiller->new();
$agent->env_proxy();

my $url = "http://packages.ubuntu.com/search?searchon=contents&keywords=" . $header ."&mode=&suite=". $release ."&arch=any";
warn "going to get $url";
$agent->get($url);

=pod
   $agent->form_number(1) if $agent->forms and scalar @{$agent->forms};
  $agent->form_number(1);
  { local $^W; $agent->current_form->value('keywords', $header); };
  $agent->submit();
=cut

my @columns = ( 'file',"package" );
  
my $table = 'HTML::TableExtract'->new('headers', [@columns]);
(my $content = $agent->content) =~ s/\&nbsp;?//g;
$table->parse($content);

=pod
	warn Dumper($table);
=cut

my @lines;
push @lines, join(', ', @columns), "\n";
foreach my $ts ($table->table_states) {
    foreach my $row ($ts->rows) {
=pod


    warn Dumper($row);
	push @lines, '>' . join(', ', @$row) . "<\n";
=cut
my $filename = @$row[0];
my $package = @$row[1];

$package =~ s/\s+//g;
print "$filename\t$package\n";

    }
}
=pod
print $content;
print (@lines);
=cut



